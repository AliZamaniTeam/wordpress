<?php
/**
 * Load parent theme style.
 */
function unite_child_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'unite_child_enqueue_styles', 11 );

// Creating a Films Custom Post Type
function films_custom_post_type() {
    $labels = array(
        'name'                => __( 'Films' ),
        'singular_name'       => __( 'Film'),
        'menu_name'           => __( 'Films'),
        'parent_item_colon'   => __( 'Parent Film'),
        'all_items'           => __( 'All Films'),
        'view_item'           => __( 'View Film'),
        'add_new_item'        => __( 'Add New Film'),
        'add_new'             => __( 'Add New'),
        'edit_item'           => __( 'Edit Film'),
        'update_item'         => __( 'Update Film'),
        'search_items'        => __( 'Search Film'),
        'not_found'           => __( 'Not Found'),
        'not_found_in_trash'  => __( 'Not found in Trash')
    );
    $args = array(
        'label'               => __( 'films'),
        'description'         => __( 'Best Films'),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
        'public'              => true,
        'hierarchical'        => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'has_archive'         => true,
        'can_export'          => true,
        'exclude_from_search' => false,
        'yarpp_support'       => true,
        //'taxonomies' 	      => array('post_tag', 'category'),
        'publicly_queryable'  => true,
        'capability_type'     => 'page'
    );
    register_post_type( 'films', $args );
}
add_action( 'init', 'films_custom_post_type', 0 );

add_action( 'init', 'create_films_custom_taxonomies', 0 );

//create custom taxonomies for films
function create_films_custom_taxonomies() {

    // Add genre taxonomy
    $labels = array(
        'name' => _x( 'Genre', 'Film genre' ),
        'singular_name' => _x( 'Genre', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Genres' ),
        'all_items' => __( 'All Genres' ),
        'parent_item' => __( 'Parent Genre' ),
        'parent_item_colon' => __( 'Parent Genre:' ),
        'edit_item' => __( 'Edit Genre' ),
        'update_item' => __( 'Update Genre' ),
        'add_new_item' => __( 'Add New Genre' ),
        'new_item_name' => __( 'New Genre Name' ),
        'menu_name' => __( 'Genres' ),
    );

    register_taxonomy('genres',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'genre' ),
    ));

    //Add Country taxonomy
    $labels = array(
        'name' => _x( 'Country', 'Film genre' ),
        'singular_name' => _x( 'Country', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Countries' ),
        'all_items' => __( 'All Countries' ),
        'parent_item' => __( 'Parent Country' ),
        'parent_item_colon' => __( 'Parent Country:' ),
        'edit_item' => __( 'Edit Country' ),
        'update_item' => __( 'Update Country' ),
        'add_new_item' => __( 'Add New Country' ),
        'new_item_name' => __( 'New Country Name' ),
        'menu_name' => __( 'Countries' ),
    );

    register_taxonomy('countries',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'country' ),
    ));

    //Add year taxonomy
    $labels = array(
        'name' => _x( 'Year', 'Film year' ),
        'singular_name' => _x( 'Year', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Years' ),
        'all_items' => __( 'All Years' ),
        'parent_item' => __( 'Parent Year' ),
        'parent_item_colon' => __( 'Parent Year:' ),
        'edit_item' => __( 'Edit Year' ),
        'update_item' => __( 'Update Year' ),
        'add_new_item' => __( 'Add New Year' ),
        'new_item_name' => __( 'New Year Name' ),
        'menu_name' => __( 'Years' ),
    );

    register_taxonomy('years',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'years' ),
    ));

    //Add actor taxonomy
    $labels = array(
        'name' => _x( 'Actor', 'Film actor' ),
        'singular_name' => _x( 'Actor', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Actors' ),
        'all_items' => __( 'All Actors' ),
        'parent_item' => __( 'Parent Actor' ),
        'parent_item_colon' => __( 'Parent Actor:' ),
        'edit_item' => __( 'Edit Actor' ),
        'update_item' => __( 'Update Actor' ),
        'add_new_item' => __( 'Add New Actor' ),
        'new_item_name' => __( 'New Actor Name' ),
        'menu_name' => __( 'Actors' ),
    );

    register_taxonomy('actors',array('films'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'actors' ),
    ));
}

function add_film_taxonomies_after_desc()
{
    // Add countries to films list
    $termID = [];
    $terms = get_the_terms(get_the_ID(), 'countries');
    if(isset($terms)) {
        ?>
        <hr>
        <strong>Country: </strong>
        <?php

        foreach ($terms as $key => $term) {
            if ($key) echo '• ';
            ?>
            <span><?php echo $term->name ?></span>
            <?php
        }
    }

    // Add genres to films list
    $termID = [];
    $terms = get_the_terms(get_the_ID(), 'genres');
    if($terms) {
        ?>
        <hr>
        <strong>Genres: </strong>
        <?php

        foreach ($terms as $key => $term) {
            if ($key) echo '• ';
            ?>
            <span><?php echo $term->name ?></span>
            <?php
        }
    }

    // Add ticket price to films list
    $ticket_price = get_field('ticket_price', get_the_ID());
    if($ticket_price){
        ?>
        <hr>
        <strong>Ticket Price: </strong>
        <?php
        echo $ticket_price;
    }

    // Add release date to films list
    $release_date = get_field('release_date', get_the_ID());
    if($release_date){
        ?>
        <hr>
        <strong>Release Date: </strong>
        <?php
        echo $release_date;
    }

    ?>
    <hr>
    <?php

}
add_action('after_film_desc_in_archive', 'add_film_taxonomies_after_desc');

// Add last 5 film shortcode
function last_5_films_func(){
        $posts = '<div class="widget"><ul>';
        $recent_films = wp_get_recent_posts(array('post_type'=>'films'));
        foreach( $recent_films as $recent ){
            $posts.= '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
        }
        $posts.='</ul></div>';
    return $posts;
}
add_shortcode( 'last-5-films', 'last_5_films_func' );

