<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/** Disable ftp request */
define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm#/(SPxOd%%N(gqTu|zur>!W*e0(^[7%@n<epHaM$ #Q8aR>[8/Xdgvb~+$IA[|C');
define('SECURE_AUTH_KEY',  'Fqvb;@:L7>%/Z9C(tk:v[fZ[1XS;]^QCEL}|W&/*Wxnn=Zyx1_JLuu1cv@_H.U(}');
define('LOGGED_IN_KEY',    'Ey0D.5+I4b<ZMP6byDf+OrhANO#sO73{~d3#%hzn#_f?ry.crm9g8kf~rTyPt*UY');
define('NONCE_KEY',        'H{v;OKg$;E$yc0_azJe-v;U;xwn]vmHyX(~;BMC!acGYys0tM0zqu(*O?Jq;?F~k');
define('AUTH_SALT',        ':SQ>WH&=%L;mrp99DZ(JvA?z=4)c:H( 1VTF?6=2(i]qDz!)qkfP)&6[T|8JM)|p');
define('SECURE_AUTH_SALT', '$WB7r62`;`oS-A&F r:.)!xZH;p#Q[L~R}`48oxnRFj4EVXlupaclYofuv-lcemq');
define('LOGGED_IN_SALT',   'UCsLrL(r#(H[y: ksYR:[W(#a|3TejX%,rkx~K2SboJQljba$3}dqb{z>[X&Q,[9');
define('NONCE_SALT',       'I-:L*{dUwlL&zw#DX,L^IgpJ%Ks9g{MBxYc$H)wMLw9H[-X3TQ_%[#g)z=@AF%U0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
